# Overview

This charm provides a basic [hubot](https://hubot.github.com/) for use with your
chat tool of choice. Hubot has adapters for [many chat platforms](https://hubot.github.com/docs/adapters/)

"Hubot is a library and an executable that wraps up all the logic of connecting
to chat servers. At his core, Hubot simply idles in various rooms and waits
for specific strings to go by in the chat....

Every Hubot is powered by npm so you can easily bring in whatever Javascript
libraries you want."[https://github.com/blog/968-say-hello-to-hubot](1)

# Usage

To use hubot with your chat platform, specify an adapter from the list above,
give your bot a nickname/alias, and tell it where to connect.

To deploy your hubot, you would do something like the following:

```
juju deploy hubot --config config.yaml
```

Or, if you'd rather set the configuration after the environment is built, you
can do it that way as well:

```
juju set hubot --config config.yaml
```

See the section titled ***Configuration*** for details on how to configure
your hubot for your chat platform of choice.

## Known Limitations and Issues

Depending on whether you deploy custom scripts to your hubot, you may be
unable to install them because of missing libraries for `npm` dependencies.

A future version of this charm will allow you to specify a list of additional
packages to install before invoking `npm install`.

# Configuration

For IRC, an example `config.yaml` would look like this:

```
hubot:
  bot_environment_variables: 'HUBOT_IRC_SERVER="irc.freenode.net",HUBOT_IRC_ROOMS="#juju",HUBOT_IRC_NICK="hubot"'
  hubot_adapter: irc
  hubot_nickname: hubot
  use_default_scripts: true
```

For Slack, the `config.yaml` would look like this:

```
hubot:
  bot_environment_variables: 'HUBOT_SLACK_TOKEN="xxxxxxxx"'
  hubot_adapter: slack
  hubot_nickname: 3bot
  use_default_scripts: true
```

Note the `bot_environment_variables` key in the IRC example above. You can use
that key to expose things like API keys, authentication information and the like
to the `supervisor` process, which looks after running your hubot.

# Contact Information

## Upstream Project Name

  - https://gitlab.org/jamonation/layer-hubot

